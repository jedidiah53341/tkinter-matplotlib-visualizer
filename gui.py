import tkinter as tk
from tkinter import *
import pandas as pd
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
pd.set_option('display.width', 2000)
import seaborn as sns
import datetime
import matplotlib.pyplot as plt
from tkinter.filedialog import askopenfilename, asksaveasfilename

def open_csv():
    """Open a file for editing."""
    filepath = askopenfilename(
        filetypes=[(".csv files", "*.csv"), ("All Files", "*.*")]
    )
    if not filepath:
        return
    global dataset
    dataset = pd.read_csv(filepath)
    text = dataset.head()
    text2 = '\nKolom dengan missing value:'
    text3 = dataset.isnull().any()
    txt_edit.delete('1.0', END)
    txt_edit.insert(tk.END, text)
    txt_edit.insert(tk.END, text2)
    txt_edit.insert(tk.END, text3)
    window.title(f"Simple Python Visualizer - {filepath}")
    
    menu = group_by_column.children["menu"]
    menu.delete(0, "end")
    columns = list(dataset.columns)
    for value in columns:
        menu.add_command(label=value, command=lambda v=value: variable1.set(v))
        
    menu = group_by_2_column.children["menu"]
    menu.delete(0, "end")
    columns = list(dataset.columns)
    for value in columns:
        menu.add_command(label=value, command=lambda v=value: variable2.set(v))
        
    menu = group_by_agg_column.children["menu"]
    menu.delete(0, "end")
    columns = list(dataset.columns)
    for value in columns:
        menu.add_command(label=value, command=lambda v=value: variable3.set(v))
def show(choice):
    choice = variable.get()
    if choice == "Line" :
        show_line_chart()
    elif choice == "Bar" :
        show_bar_chart()
    elif choice == "Barh" :
        show_barh_chart()
    elif choice == "Area" :
        show_area_chart()
    elif choice == "Pie" :
        show_pie_chart()
    elif choice == "Scatter" :
        show_scatter_plot()
    elif choice == "Multi-line" :
        show_multiline_chart();
    elif choice == "Multi-bar" :
        show_multibar_chart()
    elif choice == "Simple Scatter" :
        show_simple_chart()
    choice = variable4.get()
    if choice == "Count Plot" :
        show_countplot()
    
def show_line_chart():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].sum().reset_index()
    plt.plot(grouped_data[group_by_value],grouped_data[group_by_aggregate_value])
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_simplescatter_chart():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].count().reset_index()
    plt.plot(grouped_data[group_by_value],grouped_data[group_by_aggregate_value])
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_bar_chart():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].sum().reset_index()
    plt.bar(grouped_data[group_by_value],grouped_data[group_by_aggregate_value])
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_barh_chart():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].sum().reset_index()
    plt.barh(grouped_data[group_by_value],grouped_data[group_by_aggregate_value])
    plt.xlabel(group_by_aggregate_value)
    plt.ylabel(group_by_value)
    plt.show()
def show_area_chart():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].sum().reset_index()
    plt.fill_between(grouped_data[group_by_value],grouped_data[group_by_aggregate_value])
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_multiline_chart():
    group_by_value = variable1.get()
    group_by_and_value = variable2.get()
    group_by_aggregate_value = variable3.get()
    dataset.groupby([group_by_value,group_by_and_value])[group_by_aggregate_value].sum().unstack().plot()
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_pie_chart():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].sum().reset_index()
    plt.pie(grouped_data[group_by_aggregate_value],labels=grouped_data[group_by_value],autopct='%1.2f%%')
    plt.show()
def show_multibar_chart():
    group_by_value = variable1.get()
    group_by_and_value = variable2.get()
    group_by_aggregate_value = variable3.get()
    dataset.groupby([group_by_value,group_by_and_value])[group_by_aggregate_value].sum().unstack().plot(kind='bar')
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_scatter_plot():
    group_by_value = variable1.get()
    group_by_aggregate_value = variable3.get()
    grouped_data = dataset.groupby(group_by_value)[group_by_aggregate_value].sum().reset_index()
    plt.scatter(grouped_data[group_by_value],grouped_data[group_by_aggregate_value])
    plt.xlabel(group_by_value)
    plt.ylabel(group_by_aggregate_value)
    plt.show()
def show_countplot():
    group_by_value = variable3.get()
    group_by_aggregate_value = variable1.get()
    sns.countplot(data=dataset, x=group_by_value, hue=group_by_aggregate_value)
    plt.show()
def temporary_datetime():
    base_column = variable1.get()
    format_from = datetime_format_from.get()
    format_to = datetime_format_to.get()
    column_name = datetime_name.get()
    dataset[column_name] = dataset['order_date'].apply(lambda x: datetime.datetime.strptime(x, format_from).strftime(format_to))
    text = dataset.head()
    txt_edit.delete('1.0', END)
    txt_edit.insert(tk.END, text)

    menu = group_by_column.children["menu"]
    menu.delete(0, "end")
    columns = list(dataset.columns)
    for value in columns:
        menu.add_command(label=value, command=lambda v=value: variable1.set(v))
        
    menu = group_by_2_column.children["menu"]
    menu.delete(0, "end")
    columns = list(dataset.columns)
    for value in columns:
        menu.add_command(label=value, command=lambda v=value: variable2.set(v))
        
    menu = group_by_agg_column.children["menu"]
    menu.delete(0, "end")
    columns = list(dataset.columns)
    for value in columns:
        menu.add_command(label=value, command=lambda v=value: variable3.set(v))

window = tk.Tk()
window.title("Simple Python Visualizer")
window.rowconfigure(0, minsize=200, weight=1)
window.columnconfigure(1, minsize=800, weight=1)

txt_edit = tk.Text(window,height = 10, width = 50,wrap="none")
fr_buttons = tk.Frame(window, relief=tk.RAISED, bd=3)
fr_buttons.grid(row=0, column=0, sticky="ns")
txt_edit.grid(row=0, column=1, sticky="nsew")
txt_edit.insert(tk.END, "This is a simple matplotlib and seaborn visualization tool i made while learning Tkinter. \nIt was meant to help me generate plots without much coding.\nThis tool can only perform grouped-summed data and counted data.")
txt_edit.insert(tk.END, "\n\nTo start, open a csv file, then define the columns you want to group in c#1 and c#2.\nThe only charts that can display multiple grouped columns are multiline and multibar charts.")
txt_edit.insert(tk.END, "\nThen, pick the column that you wanted to sum -- in relation to the grouped column, in c#3. Choose the chart that you want to generate.")
txt_edit.insert(tk.END, "\n\nYou can also process a date column into monthly or yearly data using the 'generate temporary date column' section.\nJust select the base column to c#1, enter the base column's format, then the target format to it's right, then the name of the temporary column below it.")

                
textHsb = tk.Scrollbar(txt_edit, orient="horizontal", command=txt_edit.xview)
txt_edit.configure( xscrollcommand=textHsb.set)
textHsb.pack(side="bottom", fill="both" ,expand=False)

## Read CSV
btn_read_csv = tk.Button(fr_buttons, text="Read .csv", command=open_csv)
btn_read_csv.grid(row=1, column=0, sticky="ew", padx=5, pady=5)

label_create_line_chart = tk.Label(fr_buttons, text="Choose columns", font='bold')
label_create_line_chart.grid(row=2, column=0, sticky="w", padx=5)

##Define Columns
variable1 = StringVar(window)
variable1.set("c#1 / groupby1") # default value
group_by_column = OptionMenu(fr_buttons, variable1, "")
group_by_column.grid(row=3, column=0, sticky="ew", padx=5)

variable2 = StringVar(window)
variable2.set("c#2 / groupby2") # default value
group_by_2_column = OptionMenu(fr_buttons, variable2, "")
group_by_2_column.grid(row=4, column=0, sticky="ew", padx=5)

variable3 = StringVar(window)
variable3.set("c#3 / count / sum") # default value
group_by_agg_column = OptionMenu(fr_buttons, variable3, "")
group_by_agg_column.grid(row=5, column=0, sticky="ew", padx=5)

## Generate temporary date columns
label_tempdate_chart = tk.Label(fr_buttons, text="Generate temporary date columns", font='bold')
label_tempdate_chart.grid(row=6, column=0, sticky="w", padx=5)
datetime_grid = tk.Frame(fr_buttons, relief=tk.RAISED)
datetime_grid.grid(row=7, column=0, sticky="ns")
datetime_format_from = Entry(datetime_grid)
datetime_format_from.grid(row=0, column=0, sticky="ew", padx=5)
datetime_format_from.insert(END, 'from %Y-%m-%d')
datetime_format_to = Entry(datetime_grid)
datetime_format_to.grid(row=0, column=1, sticky="ew", padx=5)
datetime_format_to.insert(END, 'to %Y-%m')
datetime_name = Entry(fr_buttons)
datetime_name.grid(row=8, column=0, sticky="ew", padx=5)
datetime_name.insert(END, "Temporary grid's name : ")
generate_datetime_column = tk.Button(fr_buttons, text="Generate temporary column", command=temporary_datetime)
generate_datetime_column.grid(row=9, column=0, sticky="ew", padx=5, pady=5)

## Show Charts
label_create_which_chart = tk.Label(fr_buttons, text="Choose Charts Library", font='bold')
label_create_which_chart.grid(row=10, column=0, sticky="w", padx=5)
##Show Matplotlib Charts
variable = StringVar(window)
variable.set("Select Matplotlib Charts") # default value
w = OptionMenu(fr_buttons, variable, "Line", "Bar", "Barh", "Area",  "Pie", "Scatter", "Multi-line", "Multi-bar", "Simple Line", command=show)
w.grid(row=11, column=0, sticky="ew", padx=5)

##Show Seaborn Charts
variable4 = StringVar(window)
variable4.set("Select Seaborn Charts") # default value
seaborn_chart_select = OptionMenu(fr_buttons, variable4, "Count Plot", command=show)
seaborn_chart_select.grid(row=12, column=0, sticky="ew", padx=5)

window.mainloop()
